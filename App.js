import React from 'react';
import StackNavigator from './src/navigation/StackNavigation';

import { LogBox } from 'react-native';
LogBox.ignoreAllLogs();

const App = () => {
    return (
        <StackNavigator />
    )
}

export default App
