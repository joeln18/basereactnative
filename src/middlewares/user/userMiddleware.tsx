
import { Dispatch } from 'redux';
import { setUserCreator } from '../../redux/actions/User/UserActionsCreator';
import { User } from '../../redux/store/types';

export const setUserMiddleware = (user: User) => {
    return ((dispatch: Dispatch<any>) => {
        dispatch(setUserCreator(user))
    })
}
