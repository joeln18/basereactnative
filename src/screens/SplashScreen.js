import React, { useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';

var timerInit;

const SplashScreen = ({navigation}) => {
    console.log("navigation splash ", navigation);

    useEffect(() => {
        initApp();
        
        return () => {
           clearTimeout(timerInit);
        }
    }, [])

    const initApp = () => {
        timerInit = setTimeout(() => {
            navigation.navigate("TabPublic")
        }, 1000);
    }

    return (
       <View style={styles.container}>
           <Text style={styles.title}>SplashScreen</Text>
       </View>
    )
}

const styles = StyleSheet.create({
    container:{
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    title:{
        textAlign: 'center',
        fontWeight: 'bold'
    }
})

export default SplashScreen;
