import React, { useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  Animated,
  ScrollView,
  Dimensions,
  PixelRatio,
} from "react-native";

import * as Images from "../../../constants/ImagesIntro";
import { getScreen1Styles, getScreen2Styles, getScreen3Styles } from '../../../utils/transformAnimation';



const IntroScreen = () => {
    const animation = useRef(new Animated.Value(0)).current;
  
    const { width, height } = Dimensions.get("window");

    const screen1Styles = getScreen1Styles(animation, width);
    const screen2Styles = getScreen2Styles(animation, width);
    const screen3Styles = getScreen3Styles(animation, width);

    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          pagingEnabled
          horizontal
          scrollEventThrottle={16}
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: {
                  x: animation,
                },
              },
              
            },
          ],
          {useNativeDriver: false}
          )}
        >
          <View style={{ width, height, backgroundColor: "#F89E20" }}>
            <View style={styles.screenHeader}>
              <Animated.Image
                source={Images.Image1}
                style={{
                  width: PixelRatio.getPixelSizeForLayoutSize(75),
                  height: PixelRatio.getPixelSizeForLayoutSize(63),
                }}
                resizeMode="contain"
              />

              <Animated.Image
                source={Images.Image2}
                style={[
                  {
                    width: PixelRatio.getPixelSizeForLayoutSize(46),
                    height: PixelRatio.getPixelSizeForLayoutSize(28),
                    position: "absolute",
                    top: 200,
                    left: 60,
                  },
                  screen1Styles.image2,
                ]}
                resizeMode="contain"
              />
              <Animated.Image
                source={Images.Image3}
                style={{
                  width: PixelRatio.getPixelSizeForLayoutSize(23),
                  height: PixelRatio.getPixelSizeForLayoutSize(17),
                  position: "absolute",
                  top: 150,
                  left: 60,
                }}
                resizeMode="contain"
              />
            </View>
            <View style={styles.screenText}>
              <Text>Screen 1</Text>
            </View>
          </View>

          <View style={{ width, height, backgroundColor: "#F89E20" }}>
            <View style={styles.screenHeader}>
              <Animated.Image
                source={Images.Image1}
                style={{
                  width: PixelRatio.getPixelSizeForLayoutSize(75),
                  height: PixelRatio.getPixelSizeForLayoutSize(63),
                }}
                resizeMode="contain"
              />

              <Animated.Image
                source={Images.Image2}
                style={[
                  {
                    width: PixelRatio.getPixelSizeForLayoutSize(46),
                    height: PixelRatio.getPixelSizeForLayoutSize(28),
                    position: "absolute",
                    top: 200,
                    left: 60,
                  },
                  screen2Styles.image2,
                ]}
                resizeMode="contain"
              />
              <Animated.Image
                source={Images.Image3}
                style={{
                  width: PixelRatio.getPixelSizeForLayoutSize(23),
                  height: PixelRatio.getPixelSizeForLayoutSize(17),
                  position: "absolute",
                  top: 150,
                  left: 60,
                }}
                resizeMode="contain"
              />
            </View>
            <View style={styles.screenText}>
              <Text>Screen 2</Text>
            </View>
          </View>
          <View style={{ width, height, backgroundColor: "#F89E20" }}>
            <View style={styles.screenHeader}>
              <Animated.Image
                source={Images.Image1}
                style={[
                  {
                    width: PixelRatio.getPixelSizeForLayoutSize(75),
                    height: PixelRatio.getPixelSizeForLayoutSize(63),
                  },
                  screen3Styles.image1,
                ]}
                resizeMode="contain"
              />

              <Animated.Image
                source={Images.Image2}
                style={[
                  {
                    width: PixelRatio.getPixelSizeForLayoutSize(46),
                    height: PixelRatio.getPixelSizeForLayoutSize(28),
                    position: "absolute",
                    top: 200,
                    left: 60,
                  },
                  screen3Styles.image2,
                ]}
                resizeMode="contain"
              />
              <Animated.Image
                source={Images.Image3}
                style={{
                  width: PixelRatio.getPixelSizeForLayoutSize(23),
                  height: PixelRatio.getPixelSizeForLayoutSize(17),
                  position: "absolute",
                  top: 150,
                  left: 60,
                }}
                resizeMode="contain"
              />
            </View>
            <View style={styles.screenText}>
              <Text>Screen 3</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  screenHeader: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  screenText: {
    flex: 1,
  },
});

export default IntroScreen;
