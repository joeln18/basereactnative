import React, { useState } from "react";
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Dimensions,
  Animated
} from "react-native";
import { getRandomInt } from "../../../utils/fRandom";
import Heart from "../../components/Heart";

const { width, height } = Dimensions.get("window");

var array = [];

const HeartFloatScreen = (props) => {
    const [hearts, setHearts] = useState([]);
  
    const handleAddHeart = () => {
        const animation = new Animated.Value(0);
        
        array = [...array, {animation, start: getRandomInt(100, width - 100)}]
        setHearts(array);
        Animated
            .timing(animation, {
                toValue: height,
                duration: 3000,
                useNativeDriver: true
            })
            .start();
    }

    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={handleAddHeart}>
          <View style={StyleSheet.absoluteFill}>
            {hearts.map(({animation, start }, index) => {
              const dividedHeight = height / 6;

              const positionInterpolate = animation.interpolate({
                inputRange: [0, height],
                outputRange: [height - 50, 0]
              });

              const opacityInterpolate = animation.interpolate({
                inputRange: [0, height - 200],
                outputRange: [1, 0]
              });

              const scaleInterpolate = animation.interpolate({
                inputRange: [0, 15, 30],
                outputRange: [0, 1.2, 1],
                extrapolate: "clamp"
              });

              const wobbleInterpolate = animation.interpolate({
                inputRange: [
                  0,
                  dividedHeight * 1,
                  dividedHeight * 2,
                  dividedHeight * 3,
                  dividedHeight * 4,
                  dividedHeight * 5,
                  dividedHeight * 6,
                ],
                outputRange: [
                  0,
                  15,
                  -15,
                  15,
                  -15,
                  15,
                  -15,
                ],
                extrapolate: "clamp"
              });

              const heartStyle = {
                left: start,
                transform: [
                  { translateY: positionInterpolate },
                  { translateX: wobbleInterpolate },
                  { scale: scaleInterpolate }
                ],
                opacity: opacityInterpolate
              };

              return <Heart key={index} style={heartStyle} />;
            })}
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  
}



const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default HeartFloatScreen;

