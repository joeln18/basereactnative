import React, { useState, useRef, useEffect } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Animated,
    PanResponder,
    TouchableWithoutFeedback,
    Dimensions,
    TouchableOpacity,
  } from "react-native";
   
  import clamp from "clamp";
   
  import Cat1 from "../../../assets/cat1.jpeg";
  import Cat2 from "../../../assets/cat2.jpeg";
  import Cat3 from "../../../assets/cat3.jpeg";
  import Cat4 from "../../../assets/cat4.jpeg";
   
  const SWIPE_THRESHOLD = 120;
  const { height } = Dimensions.get("window");

   
  const KittenCatScreen = (props) => {
      const [items, setItems] = useState([
        {
            image: Cat1,
            id: 1,
            text: "Sweet Cat",
          },
          {
            image: Cat2,
            id: 2,
            text: "Sweeter Cat",
          },
          {
            image: Cat3,
            id: 3,
            text: "Sweetest Cat",
          },
          {
            image: Cat4,
            id: 4,
            text: "Aww",
          }
      ]);
      const animation = useRef(new Animated.ValueXY()).current;
      //const [animation, setAnimation] = useState(new Animated.ValueXY);
      //const [opacity, setOpacity] = useState(new Animated.Value(1));
      //const [next, setNext] = useState(new Animated.Value(0.9))
      const opacity = useRef(new Animated.Value(1)).current;
      const next = useRef(new Animated.Value(0.9)).current;
      const _panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onPanResponderMove: Animated.event([
                null,
                {
                    dx: animation.x,
                    dy: animation.y
                },
            ], {useNativeDriver: false}),
            onPanResponderRelease: (evt, { dx, vx, vy }) => {
                let velocity;
                if(vx >= 0)
                    velocity = clamp(vx, 3, 5)
                else
                    velocity = clamp(Math.abs(vx), 3, 5) * -1;
                
                if(Math.abs(dx) > SWIPE_THRESHOLD)
                    Animated.decay(animation, {
                        velocity: { x: velocity, y: vy},
                        deceleration: .98,
                        useNativeDriver: true
                    }).start(transitionNext())
                else
                    Animated.spring(animation, {
                        toValue: { x: 0, y: 0 },
                        friction: 4,
                        useNativeDriver: true
                    }).start()
            }
        })
      ).current

    const rotate = animation.x.interpolate({
        inputRange: [-200, 0, 200],
        outputRange: ["-30deg", "0deg", "30deg"],
        extrapolate: "clam"
    });

    const opacityAnimated = animation.x.interpolate({
        inputRange: [-200, 0, 200],
        outputRange: [-.5, 1, .5],
        extrapolate: "clam"
    });

    const animatedCardStyle = {
        opacity: opacityAnimated,
        transform:[
            {
                rotate
            },
            ...animation.getTranslateTransform()
        ]
    }

    const animatedImageStyle = {
        opacity: opacity
    }

    const transitionNext = () => {

        Animated.parallel([
            Animated.timing(opacity, {
                toValue: 0,
                duration: 300,
                useNativeDriver: true
            }),
            Animated.spring(next, {
                toValue: 1,
                friction: 4,
                useNativeDriver: true
            })
        ]).start(() => {
           
            setItems( prevState => prevState.slice(1))
            next.setValue(.9);
            opacity.setValue(1);
            animation.setValue({ x: 0, y: 0 })
        
        })

        
    }

    const handleNo = () => {
        Animated.timing(animation.x, {
            toValue: -SWIPE_THRESHOLD,
            useNativeDriver: true
        }).start(transitionNext())
    }

    const handleYes = () => {
        Animated.timing(animation.x, {
            toValue: SWIPE_THRESHOLD,
            useNativeDriver: true
        }).start(transitionNext())
    }
    
      return (
        <View style={styles.container}>
          <View style={styles.top}>
            {
                items.slice(0, 2).reverse().map(({ image, id, text}, index, items) => {

                    const isLastItem = index === items.length - 1;
                    const isSecondToLast = index === items.length - 2;

                    const panHandlers = isLastItem ? _panResponder.panHandlers : {};

                    const cardStyle = isLastItem ? animatedCardStyle : undefined;
                    const imageStyle = isLastItem ? animatedImageStyle : undefined;

                    const nextStyle = isSecondToLast ? {
                        transform: [{ scale: next }]
                    } : undefined;

                    return(
                        <Animated.View 
                            key={id}
                            style={[styles.card, cardStyle, nextStyle]}
                            {...panHandlers}
                            >
                            <Animated.Image
                                source={image}
                                resizeMode='cover'
                                style={[styles.image, imageStyle]}
                            />
                            <View style={styles.lowerText}>
                                <Text>{text}</Text>
                            </View>
                        </Animated.View>
                    )
                })
            }
          </View>
          <View style={styles.buttonBar}>
            <TouchableOpacity style={[styles.button, styles.nopeButton]}
                onPress={handleNo}
            >
                <Text style={styles.nopeText}>NO</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.button, styles.yupeButton]}
                onPress={handleYes}
            >
                <Text style={styles.yupText}>YES</Text>
            </TouchableOpacity>
          </View>
        </View>
      );

  }
   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    top: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
    card: {
        width: 300,
        height: 300,
        position: 'absolute',
        borderRadius: 3,
        shadowColor: "#000",
        shadowOpacity: .1,
        shadowOffset: { x: 0, y: 0},
        shadowRadius: 5,
        borderColor: '#FFF',
        borderWidth: 1
    },
    image: {
        width: null,
        height: null,
        flex: 3,
        borderRadius: 2
    },
    lowerText: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 5,
    },
    buttonBar: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    button: {
        marginHorizontal: 10, padding: 20,
        borderRadius: 30,
        alignItems:'center',
        justifyContent: 'center',
        shadowRadius: 0.3,
        shadowOffset: { x: 0, y: 0}
    },
    yupButton:{
        shadowColor: 'green'
    },
    nopeButton: {
        shadowColor: 'red'
    }
  });

  export default KittenCatScreen;