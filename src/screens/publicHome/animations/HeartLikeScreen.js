import React, { useState, useRef } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Animated
} from 'react-native';
import { getTransformAnimation } from '../../../utils/transformAnimation';

import HeartLike from "../../components/HeartLike";

const HeartLikeScreen = () => {
    const [liked, setLiked] = useState(false);
    const scale = useRef(new Animated.Value(0)).current;
    const animations = useRef([
        new Animated.Value(0),
        new Animated.Value(0),
        new Animated.Value(0),
        new Animated.Value(0),
        new Animated.Value(0),
        new Animated.Value(0),
    ]).current
  
  const triggerLike = () => {
      setLiked(!liked);
    
    const showAnimations = animations.map((animation) => {
      return Animated.spring(animation, {
        toValue: 1,
        friction: 4,
        useNativeDriver: true
      })
    })

    const hideAnimations = animations.map((animation) => {
      return Animated.timing(animation, {
        toValue: 0,
        duration: 50,
        useNativeDriver: true
      })
    }).reverse();

    Animated.parallel([
      Animated.spring(scale, {
        toValue: 2,
        friction: 3,
        useNativeDriver: true
      }),
      Animated.sequence([
        Animated.stagger(50, showAnimations),
        Animated.delay(100),
        Animated.stagger(50, hideAnimations),
      ])
    ]).start();
  }
  
    const bouncyHeart = scale.interpolate({
      inputRange: [0, 1, 2],
      outputRange: [1, .8, 1]
    });
    const heartButtonStyle = {
      transform: [{ scale: bouncyHeart }]
    }

    return (
      <View style={styles.container}>
        <View>
          <HeartLike filled style={[styles.explodeHeart, getTransformAnimation(animations[5], .4, -280, 0, "10deg", .7) ]} />
          <HeartLike filled style={[styles.explodeHeart, getTransformAnimation(animations[4], .7, -120, 40, "45deg", .5) ]} />
          <HeartLike filled style={[styles.explodeHeart, getTransformAnimation(animations[3], .8, -120, -40, "-45deg", .3) ]} />
          <HeartLike filled style={[styles.explodeHeart, getTransformAnimation(animations[2], .3, -150, 120, "-35deg", .6) ]} />
          <HeartLike filled style={[styles.explodeHeart, getTransformAnimation(animations[1], .3, -120, -120, "-35deg", .7) ]} />
          <HeartLike filled style={[styles.explodeHeart, getTransformAnimation(animations[0], .8, -60, 0, "35deg", .8) ]} />
          <TouchableWithoutFeedback onPress={triggerLike}>
            <Animated.View style={heartButtonStyle}>
              <HeartLike filled={liked} />
            </Animated.View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  explodeHeart: {
    left: 0, 
    top: 0,
    position: "absolute",
  },
});

export default HeartLikeScreen;