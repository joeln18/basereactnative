import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';


const Test1PublicScreen = ({navigation}) => {
    return (
       <View style={styles.container}>
           <Text style={styles.title}>Test1PublicScreen</Text>
           <Button title="Kitten Cat" onPress={()=>{
               navigation.navigate("KittenCat")
           }} />

            <Button title="Float Button" onPress={()=>{
               navigation.navigate("FloatButton")
            }} />
            <Button title="Intro Screen" onPress={()=>{
               navigation.navigate("Intro")
            }} />
            <Button title="Heart Float" onPress={()=>{
               navigation.navigate("HeartFloat")
            }} />
            <Button title="Heart Like" onPress={()=>{
               navigation.navigate("HeartLike")
            }} />
       </View>
    )
}

const styles = StyleSheet.create({
    container:{
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    title:{
        textAlign: 'center',
        fontWeight: 'bold',
        marginBottom: 10
    }
})

export default Test1PublicScreen;