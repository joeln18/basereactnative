import React, { useRef, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Animated,
  KeyboardAvoidingView,
  ImageBackground
} from "react-native";
 
import Background from "../../assets/background.jpg";
 
const AnimatedTextInput = Animated.createAnimatedComponent(TextInput);
 
const HomePublicScreen = (props) => {
    const email = useRef(new Animated.Value(0)).current;
    const password = useRef(new Animated.Value(0)).current;
    const button = useRef(new Animated.Value(0)).current;

    var _email;
  
    
    const createAnimationStyle = (animation) => {
        const translateY = animation.interpolate({
            inputRange: [0, 1],
            outputRange: [-5, 0],
        });
        
        return {
            opacity: animation,
            transform: [
            {
                translateY,
            },
            ],
        };
    };

    const emailStyle = createAnimationStyle(email);
    const passwordStyle = createAnimationStyle(password);
    const buttonStyle = createAnimationStyle(button);
    
    useEffect(() => {
        Animated.stagger(100, [
            Animated.timing(email, {
              toValue: 1,
              duration: 200,
              useNativeDriver: true
            }),
            Animated.timing(password, {
              toValue: 1,
              duration: 200,
              useNativeDriver: true
            }),
            Animated.timing(button, {
              toValue: 1,
              duration: 200,
              useNativeDriver: true
            }),
        ]).start(() => {
                console.log("email Ref ", _email);
                //_email.getNode().focus();
            });
    }, [])
 
    return (
      <View style={styles.container}>
          <ImageBackground 
          source={Background}
          resizeMode="cover"
          style={[StyleSheet.absoluteFill, { width: null, height: null }]}
          >
               <View style={styles.container} />
                <KeyboardAvoidingView style={styles.form} behavior="padding">
                <View style={styles.container}>
                    <Text style={styles.title}>Login</Text>
                    <AnimatedTextInput
                        //ref={email => _email = email}
                        style={[styles.input, emailStyle]}
                        placeholder="Email"
                        keyboardType="email-address"
                    />
                    <AnimatedTextInput
                        placeholder="Password"
                        style={[styles.input, passwordStyle]}
                        secureTextEntry
                    />
                    <TouchableOpacity onPress={()=>{ props.navigation.navigate("Home") }}>
                        <Animated.View style={[styles.button, buttonStyle]}>
                            <Text style={styles.buttonText}>Login</Text>
                        </Animated.View>
                    </TouchableOpacity>
                </View>
                </KeyboardAvoidingView>
                <View style={styles.container} />
          </ImageBackground>
      </View>
    );

}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 30,
    color: "#FFF",
    backgroundColor: "transparent",
    textAlign: "center",
    marginBottom: 10,
  },
  form: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,.25)",
    paddingVertical: 10,
  },
  input: {
    width: 250,
    height: 35,
    paddingHorizontal: 10,
    marginVertical: 5,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#FFF",
    color: "#333",
    backgroundColor: "#FFF",
  },
  button: {
    marginTop: 10,
    backgroundColor: "tomato",
    paddingVertical: 10,
    paddingHorizontal: 5,
    borderRadius: 5,
  },
  buttonText: {
    textAlign: "center",
    color: "#FFF",
    fontSize: 16,
  },
});
 
export default HomePublicScreen;