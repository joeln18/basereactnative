import React from 'react';
import { View, Text, StyleSheet } from 'react-native';


const Test2PrivateScreen = () => {
    return (
       <View style={styles.container}>
           <Text style={styles.title}>Test2PrivateScreen</Text>
       </View>
    )
}

const styles = StyleSheet.create({
    container:{
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    title:{
        textAlign: 'center',
        fontWeight: 'bold'
    }
})

export default Test2PrivateScreen;