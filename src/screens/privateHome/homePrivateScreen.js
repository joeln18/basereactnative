import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import { CommonActions } from '@react-navigation/native';



const HomePrivateScreen = ({ navigation }) => {
    return (
       <View style={styles.container}>
           <Text style={styles.title}>HomePrivateScreen</Text>
           <Button title="Close session" onPress={() => {
               navigation.dispatch(
                   CommonActions.reset({
                       index: 0,
                        routes: [{ name: 'Splash' }]
                   })
               )
           }} />
       </View>
    )
}

const styles = StyleSheet.create({
    container:{
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    title:{
        textAlign: 'center',
        fontWeight: 'bold',
        marginBottom: 10
    }
})

export default HomePrivateScreen;