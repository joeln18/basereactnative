import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import HomePublicScreen from '../screens/publicHome/HomePublicScreen';
import Test1PublicScreen from '../screens/publicHome/Test1PublicScreen';
import Test2PublicScreen from '../screens/publicHome/Test2PublicScreen';
import SplashScreen from '../screens/SplashScreen';
import HomePrivateScreen from '../screens/privateHome/homePrivateScreen';
import ProfileScreen from '../screens/privateHome/profileScreen';
import ConfigScreen from '../screens/privateHome/configScreen';
import Test1PrivateScreen from '../screens/privateHome/test1PrivateScreen';
import Test2PrivateScreen from '../screens/privateHome/test2PrivateScreen';
import KittenCatScreen from '../screens/publicHome/animations/KittenCatScreen';
import FloatButtonScreen from '../screens/publicHome/animations/FloatButtonScreen';
import IntroScreen from '../screens/publicHome/animations/IntroScreen';
import HeartFloatScreen from '../screens/publicHome/animations/HeartFloatScreen';
import HeartLikeScreen from '../screens/publicHome/animations/HeartLikeScreen';
//Animations Screens


const Stack = createNativeStackNavigator();

const Tab = createBottomTabNavigator();

const Drawer = createDrawerNavigator();

const TabPublicStack = () => {
    return(
        <Tab.Navigator  screenOptions={{ headerShown: false }}>
            <Tab.Screen name="HomePublic" component={HomePublicScreen} />
            <Tab.Screen name="Test1Public" component={Test1PublicScreen} />
            <Tab.Screen name="Test2Public" component={Test2PublicScreen} />
        </Tab.Navigator>
    )
}

const PublicStack = () => {
    return(
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Splash" component={SplashScreen}  />
            <Stack.Screen name="TabPublic" component={TabPublicStack} />
            <Stack.Screen name="Home" component={DrawerStack} />
            <Stack.Screen name="KittenCat" component={KittenCatScreen} />
            <Stack.Screen name="FloatButton" component={FloatButtonScreen} />
            <Stack.Screen name="Intro" component={IntroScreen} />
            <Stack.Screen name="HeartFloat" component={HeartFloatScreen} /> 
            <Stack.Screen name="HeartLike" component={HeartLikeScreen} />

        </Stack.Navigator>
    )
}

const DrawerStack = () => {
    return(
        <Drawer.Navigator>
            <Drawer.Screen name="HomePrivate" component={PrivateStack} />
            <Drawer.Screen name="Profile" component={ProfileScreen} />
            <Drawer.Screen name="Config" component={ConfigScreen} />
        </Drawer.Navigator>
    )
}

const PrivateStack = () => {
    return(
        <Tab.Navigator  screenOptions={{ headerShown: false }}>
            <Tab.Screen name="PrincipalHome" component={HomePrivateScreen} />
            <Tab.Screen name="Test1Private" component={Test1PrivateScreen} />
            <Tab.Screen name="Test2Private" component={Test2PrivateScreen} />
        </Tab.Navigator>
    )
}



const StackNavigator = () => {

    return(
        <NavigationContainer>
            <PublicStack />
        </NavigationContainer>
    )


}

export default StackNavigator;