import { setUser } from '../../reducers/user';
import { User } from '../../store/types';
import { UserActionTypes, SetUserAction  } from './UserActions';

export const setUserCreator = (payload: User): SetUserAction => ({
    type: UserActionTypes.getUser,
    payload,
    reducer: setUser
});