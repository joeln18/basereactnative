import { Action } from 'redux';

/***
 * 
 * @interface ActionBuilder
 * @template T: Type of action
 * @template P: Payload object
 * @template S: Appstate
 * 
 * @member {T} type
 * @member {object} payload
 * @member {function} reducer
 * 
 */

export interface ActionBuilder<T, P, S> extends Action {
    type: T;
    payload: P;
    reducer: ( state: S, payload: P ) => S;
}

export interface PayloadId {
    id: number
}