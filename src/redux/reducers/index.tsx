import { ActionBuilder } from '../actions/types';
import { AppState, initialState  } from '../store/types';

/**
 * 
 * @param {object} state 
 * @param {ActionBuilder} action
 * @returns {object}
 */

export const reducer = (
    state: AppState = initialState,
    action: ActionBuilder<string, any, AppState>
): AppState => (action.reducer ? action.reducer(state, action.payload) : state);