import AsyncStorage from "@react-native-async-storage/async-storage";
import { encryptTransform } from "redux-persist-transform-encrypt";
import { composeWithDevTools } from "redux-devtools-extension";
import { applyMiddleware, createStore } from "redux"; 
import { persistReducer, persistStore } from "redux-persist";
import thunk from "redux-thunk";
import { reducer } from "../reducers";
import { deviceID } from "../../config";


const encryptor = encryptTransform({
    secretKey: deviceID,
    onError: (error) => {
        if(__DEV__)console.log("Error encrypt Store " + error);
    }
});

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    transforms: [encryptor]
};

const perstReducer = persistReducer(persistConfig, reducer);

const middleware = applyMiddleware(thunk);

const store = createStore(perstReducer, composeWithDevTools(middleware));

const persistor = persistStore(store);

export { store, persistor };
