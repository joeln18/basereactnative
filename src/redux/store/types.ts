
export interface User {
    name: string;
    id: string;
}

export interface AppState {
    user: User;
}

export const initialState: AppState = {
    user: {
        name: '',
        id: ''
    }
}