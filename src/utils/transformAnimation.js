export const getScreen1Styles = (animation, width) => {
    const image2TranslateX = animation.interpolate({
      inputRange: [0, width],
      outputRange: [0, -100],
      extrapolate: "clamp",
    });
  
    return {
      image2: {
        transform: [
          {
            translateX: image2TranslateX,
          },
        ],
      },
    };
  };
  
  export const getScreen2Styles = (animation, width) => {
    const inputRange = [0, width, width * 2];
  
    const image2TranslateY = animation.interpolate({
      inputRange,
      outputRange: [100, 0, -100],
      extrapolate: "clamp",
    });
    const image2Opacity = animation.interpolate({
      inputRange,
      outputRange: [0, 1, 0],
      extrapolate: "clamp",
    });
  
    return {
      image2: {
        opacity: image2Opacity,
        transform: [
          {
            translateY: image2TranslateY,
          },
        ],
      },
    };
  };
  
  export const getScreen3Styles = (animation, width) => {
    const inputRange = [width, width * 2, width * 3];
  
    const image1Scale = animation.interpolate({
      inputRange,
      outputRange: [0, 1, 0],
      extrapolate: "clamp",
    });
  
    const image2Rotate = animation.interpolate({
      inputRange,
      outputRange: ["-180deg", "0deg", "180deg"],
      extrapolate: "clamp",
    });
  
    return {
      image1: {
        transform: [
          {
            scale: image1Scale,
          },
        ],
      },
      image2: {
        transform: [
          {
            scale: image1Scale,
          },
          {
            rotate: image2Rotate,
          },
        ],
      },
    };
  };


  export const getTransformAnimation = (animation, scale, y, x, rotate, opacity) => {
    const scaleAnimation = animation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, scale]
    })
    const xAnimatoin = animation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, x]
    })
  
    const yAnimation = animation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, y]
    })
  
    const rotationAnimation = animation.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", rotate]
    })
  
    const opacityAnimation = animation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, opacity]
    })
  
    return {
      opacity: opacityAnimation,
      transform: [
        { scale: scaleAnimation },
        { translateX: xAnimatoin },
        { translateY: yAnimation },
        { rotate: rotationAnimation }
      ]
    }
  }